#!/bin/sh

BUILDDIR=unibuild
CCFLAGS="-O2 -march=native"
CPPFLAGS="-O2 -march=native"
PREFIX=/usr
SYSCONFDIR=/etc
LOCALSTATEDIR=/var

mkdir -p $BUILDDIR
cd $BUILDDIR

case $1 in
gnu)
	../configure --prefix=$PREFIX --sysconfdir=$SYSCONFDIR --localstatedir=$LOCALSTATEDIR CFLAGS="$CCFLAGS" CXXFLAGS="$CPPFLAGS" ${*:2}
	;;
cmake)
	cmake -DCMAKE_INSTALL_PREFIX=$PREFIX -DCMAKE_BUILD_TYPE="Release" -DCMAKE_C_FLAGS="$CCFLAGS" -DCMAKE_CXX_FLAGS="$CPPFLAGS" ${*:2} ..
	;;
meson)
	meson -Dprefix=$PREFIX -Dsharedstatedir=$LOCALSTATEDIR/lib -Dsysconfdir=$SYSCONFDIR -Dlocalstatedir=$LOCALSTATEDIR -Dbuildtype=release ${*:2} . ..
	;;
*)
	echo 'Unsupported build system'
	echo 'uniconf.sh gnu|cmake|meson [additional options]'
	exit 1
	;;
esac

if test $? -eq 0
then
	echo 'Now run make in ./unibuild'
fi
