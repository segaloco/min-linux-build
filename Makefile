# Minimal Linux build
# ===================
#
# This is a minimal Linux system based on toybox, the user is root, the password is root

# Packages
# bootloader		- https://github.com/raspberrypi/firmware
# linux			- https://github.com/raspberrypi/linux
# linux-firmware	- https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git
# musl			- https://musl.libc.org/
# man-pages		- https://www.kernel.org/doc/man-pages/
# man-pages-posix	- https://git.kernel.org/pub/scm/docs/man-pages/man-pages-posix.git
# sysvinit		- https://savannah.nongnu.org/projects/sysvinit
# dash			- http://gondor.apana.org.au/~herbert/dash/
# toybox		- http://landley.net/toybox/

ARCH		= aarch64-unknown-linux-musl
KARCH		= arm64
ARCH_CFLAGS	= -march=armv8-a -mcpu=cortex-a72 -mtune=cortex-a72

CC		= $(ARCH)-gcc
CFLAGS		= -O2 -pipe $(ARCH_CFLAGS)

GNU_FLAGS	= --prefix=/usr --sysconfdir=/etc --localstatedir=/var --host=$(ARCH) CFLAGS="$(CFLAGS)"

MAKE_FLAGS	= -j4

DESTDIR		= $(HOME)/$(ARCH)

all: linux_build musl_build sysvinit_build dash_build toybox_build

clone: firmware linux linux-firmware musl man-pages man-pages-posix sysvinit dash toybox

install: firmware_install linux_install linux-firmware_install musl_install man-pages_install man-pages-posix_install sysvinit_install dash_install toybox_install

clean: linux_clean musl_clean sysvinit_clean dash_clean toybox_clean

# firmware

firmware:
	git clone https://github.com/raspberrypi/firmware.git

firmware_install: firmware
	mkdir -p $(DESTDIR)/boot
	cd firmware/boot && cp LICENCE.broadcom bootcode.bin fixup*.dat start*.elf $(DESTDIR)/boot

# linux

linux:
	git clone https://github.com/raspberrypi/linux.git

linux_build: linux
	cd linux && make ARCH=$(KARCH) defconfig && make $(MAKE_FLAGS) ARCH=$(KARCH) CROSS_COMPILE=$(ARCH)- KCFLAGS="$(CFLAGS)"

# Note, the below install command does not put everything where it needs to be
# the overlays folder and bcm2711 dtbs must be in /boot and the kernel must be named kernel8.img
# TODO: Ensure kernel bits are placed where the bootloader expects them or create symlinks
linux_install: linux_build
	cd linux && make ARCH=$(KARCH) INSTALL_PATH=$(DESTDIR)/boot INSTALL_HDR_PATH=$(DESTDIR)/usr INSTALL_MOD_PATH=$(DESTDIR) install headers_install modules_install dtbs_install
	mkdir -p $(DESTDIR)/dev
	mkdir -p $(DESTDIR)/dev/pts
	mkdir -p -m 777 $(DESTDIR)/dev/shm
	mkdir -p $(DESTDIR)/proc
	mkdir -p $(DESTDIR)/sbin
	mkdir -p $(DESTDIR)/sys

linux_clean:
	cd linux && make clean

# linux-firmware

linux-firmware:
	git clone https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git

linux-firmware_install: linux-firmware
	cd linux-firmware && make DESTDIR=$(DESTDIR) install

# musl

musl:
	git clone git://git.musl-libc.org/musl

musl_build: musl
	cd musl && ./configure $(GNU_FLAGS) && make $(MAKE_FLAGS)

musl_install: musl_build
	cd musl && make DESTDIR=$(DESTDIR) install
	mkdir -p $(DESTDIR)/bin
	mkdir -p $(DESTDIR)/etc
	mkdir -p $(DESTDIR)/run
	mkdir -p -m 777 $(DESTDIR)/tmp
	mkdir -p $(DESTDIR)/var
	ln -sfn /run $(DESTDIR)/var/run
	echo "root:x:0:0::/:" > $(DESTDIR)/etc/passwd
	echo "root:x:0:" > $(DESTDIR)/etc/group
	echo "root:$6$SUR5FWWWKdfDRIsX$t8WG.btBP.6iC4D2T2PHca0VY6kXmpb/76AxDdArYxqJbkIwNVzNR6wA97a6FBVWeF/MC9a/iCrT8Mk6yQfdh1:0:0:99999:7:::" > $(DESTDIR)/etc/shadow # "root"

musl_clean:
	cd musl && make clean

# man-pages

man-pages:
	git clone https://git.kernel.org/pub/scm/docs/man-pages/man-pages.git

man-pages_install: man-pages
	cd man-pages && make DESTDIR=$(DESTDIR) prefix=/usr install

# man-pages-posix

man-pages-posix:
	git clone https://git.kernel.org/pub/scm/docs/man-pages/man-pages-posix.git

man-pages-posix_install: man-pages-posix
	cd man-pages-posix/man-pages-posix-2017/ && make DESTDIR=$(DESTDIR) install

# sysvinit

sysvinit:
	git clone https://git.savannah.gnu.org/git/sysvinit.git

sysvinit_build: sysvinit
	cd sysvinit && make $(MAKE_FLAGS) CC=$(CC) CFLAGS="$(CFLAGS)"

sysvinit_install: sysvinit_build
	cd sysvinit && make ROOT=$(DESTDIR) install
	echo "i3:3:initdefault:" > $(DESTDIR)/etc/inittab
	echo "sh:3:respawn:/bin/sh" > $(DESTDIR)/etc/inittab
	ln -sfn /dev/initctl $(DESTDIR)/run/initctl

sysvinit_clean:
	cd sysvinit && make clean

# dash

dash:
	git clone https://git.kernel.org/pub/scm/utils/dash/dash.git

dash_build: dash
	cd dash && ./autogen.sh && ./configure $(GNU_FLAGS) && make $(MAKE_FLAGS)

dash_install: dash_build
	cd dash && make DESTDIR=$(DESTDIR) install
	ln -sfn /usr/bin/dash $(DESTDIR)/bin/sh
	ln -sfn dash.1 $(DESTDIR)/usr/share/man/man1/sh.1

dash_clean:
	cd dash && make clean

# toybox

toybox:
	git clone https://github.com/landley/toybox.git

toybox_build: toybox
	cd toybox && make defconfig && make CROSS_COMPILE=$(ARCH)- CFLAGS="$(CFLAGS)"

toybox_install: toybox_build
	cd toybox && make PREFIX=$(DESTDIR) install

toybox_clean:
	cd toybox && make clean
